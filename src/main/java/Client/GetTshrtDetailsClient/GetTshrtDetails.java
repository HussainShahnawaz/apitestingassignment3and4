package Client.GetTshrtDetailsClient;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetTshrtDetails {

    public int getTshrtDetails(String tshrtdetailsApi, String query){

        Response responsAsTshrtCollection = given()
                .queryParam("page",query)
                .when()
                .get(tshrtdetailsApi);
            System.out.println(responsAsTshrtCollection.body().asString());
                return responsAsTshrtCollection.statusCode();


    }


}
