package Client.PostUserDetailsClient;

import Client.PostUserDetailsClient.PostRequest.PostUserRequest;
import Client.PostUserDetailsClient.PostRespons.PostUserDetailsRespons;
import com.google.gson.Gson;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class PostUserDetails {


    public void postuserDetails(PostUserRequest postUserRequest , String postUserDetailsApi){
        Response GettingResponse = given()
                .when()
                .body(new Gson().toJson(postUserRequest))
                .post(postUserDetailsApi);
        PostUserDetailsRespons postUserDetailsRespons = new Gson().fromJson(GettingResponse.body().asString(),PostUserDetailsRespons.class);
        System.out.println(postUserDetailsRespons.getId());


    }
}
