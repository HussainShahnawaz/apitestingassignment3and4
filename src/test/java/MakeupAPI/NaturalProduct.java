package MakeupAPI;

import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class NaturalProduct {

    @Test

    public void findNaturalProduct() throws ParseException {

        Response response = given()
                .when()
                .get("http://makeup-api.herokuapp.com/api/v1/products.json");
        JSONParser parsejson = new JSONParser();
        Object obj = parsejson.parse(response.asString());
        JSONArray listArray = (JSONArray) obj;
        ArrayList<String> brand = new ArrayList<>();

        for (int i = 0; i < listArray.size(); i++) {
            JSONObject list = (JSONObject) listArray.get(i);
            String prodctdiscription = (String) list.get("description");
            if(prodctdiscription!=null){
                if (prodctdiscription.contains("natural")) {



                    String ItemName = (String) list.get("name");
                    brand.add(ItemName);

                }

            }

        }
        System.out.println("Unique Brands are:");
        for (String b : brand) {
            System.out.println(b);
        }
    }
}
