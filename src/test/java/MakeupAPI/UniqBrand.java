package MakeupAPI;

import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import static io.restassured.RestAssured.given;

public class UniqBrand {

    @Test

    public void findUniqBrand() throws ParseException {

        Response response = given()
                .when()
                .get("http://makeup-api.herokuapp.com/api/v1/products.json");
        JSONParser parsejson = new JSONParser();
        Object obj=parsejson.parse(response.asString());
        JSONArray listArray=(JSONArray)obj;
        ArrayList<String> brand=new ArrayList<>();

        for (int i=0;i<listArray.size();i++){
            JSONObject list = (JSONObject) listArray.get(i);
            String brandName= (String) list.get("brand");
            brand.add(brandName);



        }
        Set<String> uniqueBrand = new LinkedHashSet<>(brand);

        System.out.println("Unique Brands are:");

        for (String b:uniqueBrand) {
            System.out.println(b);
        }

    }


}
