package WetherApi;

import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class WetherReportApi {

    @Test
    public void wetherapitest() throws ParseException {
        Response response=given()
                .when()
                .headers("x-rapidapi-key","a5fcaea706msh56ddb887aca3d07p19ae6cjsn21b88868bb24")
                .headers("x-rapidapi-host","community-open-weather-map.p.rapidapi.com")
                .get("https://community-open-weather-map.p.rapidapi.com/find?q=london&cnt=1&mode=null&lon=0&type=link%2C%20accurate&lat=0&units=imperial%2C%20metric");

                JSONParser p= new JSONParser();
                Object obj=p.parse(response.asString());
                JSONObject jObj =  (JSONObject)obj;
                JSONArray listArray=(JSONArray)jObj.get("list");

                JSONObject list = (JSONObject) listArray.get(0);
                JSONArray weatherArray=(JSONArray)list.get("weather");

                JSONObject weather=(JSONObject)weatherArray.get(0);
                System.out.println(weather.get("description") +"\n" +weather.get("id"));


    }


}
